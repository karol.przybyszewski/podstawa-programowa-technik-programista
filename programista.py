import os,glob,subprocess,json,sys

header = r'''\documentclass[10pt,a4paper]{report}
\usepackage{anyfontsize}
\usepackage[polish]{babel}
\usepackage[utf8]{inputenc}
\usepackage{lmodern}
\usepackage[T1]{fontenc}
\usepackage{polski}
\frenchspacing
\usepackage{graphicx}
\usepackage{color}   %May be necessary if you want to color links
\usepackage{hyperref}
\hypersetup{
    colorlinks=true, %set true if you want colored links
    linktoc=all,     %set to all if you want both sections and subsections linked
    linkcolor=blue,  %choose some color if you want links to stand out
}

\usepackage{fancyhdr}
 
\pagestyle{fancy}
\fancyhf{}
\fancyhead[LE,RO]{Technik Programista}
\fancyhead[RE,LO]{Podstawa programowa}
\fancyfoot[CE,CO]{\leftmark}
\fancyfoot[LE,RO]{\thepage}
 
\renewcommand{\headrulewidth}{2pt}
\renewcommand{\footrulewidth}{1pt}

\begin{document}
\begin{titlepage}
  \centering
  \includegraphics[width=0.15\textwidth]{logo_kw_495.png}\par\vspace{1cm}
  {\scshape\LARGE Technikum Programistyczne INFOTECH \par}
  \vspace{1cm}
  {\scshape\Large Materiały dydaktyczne\par}
  \vspace{1.5cm}
  {\huge\bfseries Technik Programista\par}
  \vspace{2cm}
  {\Large\itshape Podstawa programowa\par}
  \vfill
  nadzór treści\par
  Karol \textsc{Przybyszewski}\par \href{mailto:karol.przybyszewski@infotech.edu.pl}{karol.przybyszewski@infotech.edu.pl} 
  \vfill
% Bottom of the page
  {\large \today\par}
\end{titlepage}
\tableofcontents
\newpage

'''

footer = r'''\end{document}'''

with open("programista.json", mode="r", encoding="utf-8") as read_file:
    zawod = json.load(read_file)

main = ''

for klasyfikacja in zawod['klasyfikacje']:
    main += r"\part{"+klasyfikacja["nazwa"]+"}"+os.linesep
    for podklasyfikacja in klasyfikacja['podklasyfikacje']:
        main += r"\chapter{"+podklasyfikacja["nazwa"]+"}"+os.linesep
        for efekt in podklasyfikacja["efekty"]:
            main += r"Uczeń "+efekt['nazwa']+":"+os.linesep
            main += r"\begin{enumerate}"+os.linesep
            for kryterium in efekt["kryteria"]:
                main += r"\item " + kryterium["nazwa"].replace('#','\#')+os.linesep
            main +=r'\end{enumerate}'+os.linesep;
            main +=r'\bigskip'+os.linesep;

content = header + main + footer

with open('programista.tex',mode='w', encoding="utf-8") as f:
     f.write(content)
'''
commandLine = subprocess.Popen(['pdflatex', 'programista.tex'])
commandLine.communicate()
os.unlink('programista.aux')
os.unlink('programista.log')
os.unlink('programista.tex')
'''